Para executar o programa deve-se seguir os passos abaixo:

1- Abra o NetBeans e clique em Arquivo/Abrir Projeto/"Selecione o projeto no local salvo".
2- No projeto aberto clique em ToComFome/Pacotes de Códigos-Fonte/tocomfome/ToComFome.java
3- Execute a classe aberta.
4- Dentro do programa Faça o login com o usuário "Funcionario" e senha "incrivel".

O programa possui uma tela principal com os menus:

Arquivo;
Cadastrar;
Transações;

No menu "Arquivo" existe um submenu chamado "Sair" para sair do programa e retornar a tela de login.
No menu "Cadastrar" existem os submenus "Cliente" e "Produto".

a) No submenu Cliente é possível cadastrar um novo cliente, visualizar os clientes cadastrados,
pesquisar por clientes, modificar o cadastro de um cliente existente e excluir clientes existentes.
b) No submenu Produto é possível cadastrar um novo produto, visualizar produtos cadastrados, 
pesquisar por produtos, modificar o cadastro de um produto existente e excluir um produto existente.

No menu "Transações" existe o submenu "Vender".

a) No submenu vender é possível pesquisar um cliente para comprar, escolher os produtos e adicionar ao pedido,
retirar o pedido, selecionar a forma de pagamento e realizar a venda. 
