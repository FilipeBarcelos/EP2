/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocomfome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class CadastroProduto extends javax.swing.JFrame {

    public CadastroProduto() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelNome = new javax.swing.JLabel();
        labelTipo = new javax.swing.JLabel();
        labelQuantidade = new javax.swing.JLabel();
        labelPreco = new javax.swing.JLabel();
        sair = new javax.swing.JButton();
        textoNome = new javax.swing.JTextField();
        textoQuantidade = new javax.swing.JTextField();
        textoPreco = new javax.swing.JTextField();
        adicionar = new javax.swing.JButton();
        excluir = new javax.swing.JButton();
        pesquisar = new javax.swing.JButton();
        limpar = new javax.swing.JButton();
        listarProdutos = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaProduto = new javax.swing.JTable();
        selecionaTipo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Produtos");
        setResizable(false);

        labelNome.setText("Nome:");

        labelTipo.setText("Tipo:");

        labelQuantidade.setText("Quantidade:");

        labelPreco.setText("Preço:");

        sair.setText("Sair");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });

        textoNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoNomeActionPerformed(evt);
            }
        });

        adicionar.setText("Adicionar");
        adicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarActionPerformed(evt);
            }
        });

        excluir.setText("Excluir");
        excluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirActionPerformed(evt);
            }
        });

        pesquisar.setText("Pesquisar");
        pesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesquisarActionPerformed(evt);
            }
        });

        limpar.setText("Limpar");
        limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limparActionPerformed(evt);
            }
        });

        listarProdutos.setText("Listar Produtos");
        listarProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listarProdutosActionPerformed(evt);
            }
        });

        tabelaProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Tipo", "Quantidade", "Preço"
            }
        ));
        jScrollPane1.setViewportView(tabelaProduto);

        selecionaTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Prato", "Bebida", "Sobremesa" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(listarProdutos)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(labelPreco)
                                .addComponent(labelQuantidade)
                                .addComponent(labelTipo)
                                .addComponent(labelNome))
                            .addComponent(sair, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(adicionar)
                                .addGap(18, 18, 18)
                                .addComponent(excluir, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(selecionaTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(textoNome, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(textoPreco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                                            .addComponent(textoQuantidade, javax.swing.GroupLayout.Alignment.LEADING)))
                                    .addComponent(limpar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(textoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTipo)
                    .addComponent(selecionaTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelQuantidade)
                    .addComponent(textoQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPreco)
                    .addComponent(textoPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adicionar)
                    .addComponent(excluir)
                    .addComponent(pesquisar))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limpar)
                    .addComponent(sair))
                .addGap(30, 30, 30)
                .addComponent(listarProdutos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void textoNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoNomeActionPerformed
       
    }//GEN-LAST:event_textoNomeActionPerformed

    private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
        this.dispose();
    }//GEN-LAST:event_sairActionPerformed

    private void adicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarActionPerformed
        Produto produto = new Produto(textoNome.getText(), selecionaTipo.getSelectedItem().toString(), Integer.parseInt(textoQuantidade.getText()), Float.parseFloat(textoPreco.getText()));    
        
        textoNome.setText("");
        selecionaTipo.setSelectedItem("Prato");
        textoQuantidade.setText("");
        textoPreco.setText("");
        
        try {
        PrintWriter arquivo = new PrintWriter("produtos/"+produto.getNome()+".txt");
        arquivo.println(produto.getNome());
        arquivo.println(produto.getTipo());
        arquivo.println(produto.getQuantidade());
        arquivo.println(produto.getPreco());
        arquivo.close();
        JOptionPane.showMessageDialog(null, "Produto adicionado com sucesso!");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Produto não pode ser adicionado!");
        }
    }//GEN-LAST:event_adicionarActionPerformed

    private void excluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirActionPerformed
        Produto produto = new Produto(textoNome.getText(), selecionaTipo.getSelectedItem().toString(), Integer.parseInt(textoQuantidade.getText()), Float.parseFloat(textoPreco.getText()));
        
        File arquivo = new File("produtos/"+produto.getNome()+".txt");
        arquivo.delete();
        JOptionPane.showMessageDialog(null, "Produto excluído com sucesso!");
        
        textoNome.setText("");
        selecionaTipo.setSelectedItem("Prato");
        textoQuantidade.setText("");
        textoPreco.setText("");
    }//GEN-LAST:event_excluirActionPerformed

    private void pesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pesquisarActionPerformed

        String novoNome = JOptionPane.showInputDialog(null, "Digite o nome do produto:");
        try {
            if (novoNome == null) {
                JOptionPane.showMessageDialog(null, "Ação cancelada!");
            } else {
                BufferedReader arquivo = new BufferedReader(new FileReader("produtos/"+novoNome+".txt"));
                textoNome.setText(arquivo.readLine());
                selecionaTipo.setSelectedItem(arquivo.readLine());
                textoQuantidade.setText(arquivo.readLine());
                textoPreco.setText(arquivo.readLine());
                arquivo.close();
            }
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Produto não encontrado!");
        }
    }//GEN-LAST:event_pesquisarActionPerformed

    private void limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limparActionPerformed

        textoNome.setText("");
        selecionaTipo.setSelectedItem("Prato");
        textoQuantidade.setText("");
        textoPreco.setText("");        
    }//GEN-LAST:event_limparActionPerformed

    private void listarProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listarProdutosActionPerformed
              Produto produto = new Produto();
        DefaultTableModel dtmProduto = (DefaultTableModel) tabelaProduto.getModel();
        dtmProduto.setNumRows(0);
        
        File arquivos[];
        File diretorio = new File("produtos");
        arquivos = diretorio.listFiles();
        for(int i = 0; i < arquivos.length; i++){
            try {
            FileReader fr = new FileReader(arquivos[i]);
            BufferedReader leitura = new BufferedReader(fr);
            produto.setNome(leitura.readLine());
            produto.setTipo(leitura.readLine());
            produto.setQuantidade(Integer.parseInt(leitura.readLine()));
            produto.setPreco(Float.parseFloat(leitura.readLine()));
            
            Object[] dados = {produto.getNome(), produto.getTipo(), produto.getQuantidade(), produto.getPreco()};
            dtmProduto.addRow(dados);
            } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Produtos não encontrados!");
            }
        }
    }//GEN-LAST:event_listarProdutosActionPerformed

    public static void main(String args[]) {
   
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroProduto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adicionar;
    private javax.swing.JButton excluir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelNome;
    private javax.swing.JLabel labelPreco;
    private javax.swing.JLabel labelQuantidade;
    private javax.swing.JLabel labelTipo;
    private javax.swing.JButton limpar;
    private javax.swing.JButton listarProdutos;
    private javax.swing.JButton pesquisar;
    private javax.swing.JButton sair;
    private javax.swing.JComboBox<String> selecionaTipo;
    private javax.swing.JTable tabelaProduto;
    private javax.swing.JTextField textoNome;
    private javax.swing.JTextField textoPreco;
    private javax.swing.JTextField textoQuantidade;
    // End of variables declaration//GEN-END:variables
}
