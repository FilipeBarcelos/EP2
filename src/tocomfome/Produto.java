
package tocomfome;


public class Produto {
   private String nome;
   private String tipo;
   private int quantidade;
   private float preco;
   
   public Produto() {
       
   }
   public Produto(String nome, String tipo, int quantidade, float preco) {
       this.nome = nome;
       this.tipo = tipo;
       this.quantidade = quantidade;
       this.preco = preco;
   }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }
   
}
