
package tocomfome;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Vender extends javax.swing.JFrame {


    public Vender() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelNome = new javax.swing.JLabel();
        labelTelefone = new javax.swing.JLabel();
        labelEndereco = new javax.swing.JLabel();
        textoNome = new javax.swing.JTextField();
        textoTelefone = new javax.swing.JTextField();
        textoEndereco = new javax.swing.JTextField();
        selecionar = new javax.swing.JButton();
        adicionarVenda = new javax.swing.JButton();
        labelProduto = new javax.swing.JLabel();
        textoProduto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaVenda = new javax.swing.JTable();
        labelLista = new javax.swing.JLabel();
        selecionarQuantidade = new javax.swing.JSpinner();
        labelQuantidade = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();
        textoTotal = new javax.swing.JTextField();
        labelTroco = new javax.swing.JLabel();
        textoTroco = new javax.swing.JTextField();
        tipoPagamento = new javax.swing.JComboBox<>();
        textoReceber = new javax.swing.JTextField();
        labelReceber = new javax.swing.JLabel();
        cancelar = new javax.swing.JButton();
        finalizar = new javax.swing.JButton();
        limpar = new javax.swing.JButton();
        gerarTroco = new javax.swing.JButton();
        retirar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Realizar Venda");
        setPreferredSize(new java.awt.Dimension(572, 736));
        setResizable(false);

        labelNome.setText("Nome:");

        labelTelefone.setText("Telefone:");

        labelEndereco.setText("Endereço:");

        textoNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoNomeActionPerformed(evt);
            }
        });

        textoTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoTelefoneActionPerformed(evt);
            }
        });

        textoEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoEnderecoActionPerformed(evt);
            }
        });

        selecionar.setText("Selecionar Cliente");
        selecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selecionarActionPerformed(evt);
            }
        });

        adicionarVenda.setText("Adicionar ao Pedido");
        adicionarVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarVendaActionPerformed(evt);
            }
        });

        labelProduto.setText("Produto:");

        listaVenda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Tipo", "Quantidade", "Valor Unidade", "Valor Total"
            }
        ));
        jScrollPane1.setViewportView(listaVenda);

        labelLista.setText("Lista do Pedido:");

        labelQuantidade.setText("Quantidade:");

        labelTotal.setText("Total à Pagar:");

        textoTotal.setText(" ");

        labelTroco.setText("Troco de:");

        textoTroco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoTrocoActionPerformed(evt);
            }
        });

        tipoPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cartão Débito", "Cartão Crédito", "Dinheiro" }));

        labelReceber.setText("Troco para:");

        cancelar.setText("Cancelar Venda");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });

        finalizar.setText("Finalizar Venda");
        finalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizarActionPerformed(evt);
            }
        });

        limpar.setText("Limpar");
        limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limparActionPerformed(evt);
            }
        });

        gerarTroco.setText("Gerar Troco");
        gerarTroco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gerarTrocoActionPerformed(evt);
            }
        });

        retirar.setText("Retirar do Pedido");
        retirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retirarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 300, Short.MAX_VALUE)
                        .addComponent(finalizar))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tipoPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelTotal)
                                    .addComponent(labelReceber)
                                    .addComponent(labelTroco))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(textoTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                                    .addComponent(textoReceber)
                                    .addComponent(textoTroco)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(8, 8, 8)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(labelQuantidade)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(selecionarQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(labelEndereco)
                                                .addComponent(labelTelefone)
                                                .addComponent(labelNome)
                                                .addComponent(labelProduto))
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(textoProduto)
                                                .addComponent(textoTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                                                .addComponent(textoNome)
                                                .addComponent(textoEndereco, javax.swing.GroupLayout.Alignment.TRAILING)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(retirar)
                                            .addGap(64, 64, 64)
                                            .addComponent(adicionarVenda))))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(selecionar)
                                    .addGap(18, 18, 18)
                                    .addComponent(limpar))
                                .addComponent(labelLista))
                            .addComponent(gerarTroco, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selecionar)
                    .addComponent(limpar))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(textoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelTelefone)
                    .addComponent(textoTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textoEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelEndereco))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelProduto)
                    .addComponent(textoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selecionarQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelQuantidade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adicionarVenda)
                    .addComponent(retirar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelLista)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelTotal)
                        .addComponent(textoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tipoPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textoReceber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelReceber))
                .addGap(18, 18, 18)
                .addComponent(gerarTroco)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textoTroco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTroco))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelar)
                    .addComponent(finalizar))
                .addGap(23, 23, 23))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void textoEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoEnderecoActionPerformed
        
    }//GEN-LAST:event_textoEnderecoActionPerformed

    private void textoTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoTelefoneActionPerformed
       
    }//GEN-LAST:event_textoTelefoneActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        JOptionPane.showMessageDialog(null, "Venda cancelada!");
        this.dispose();
    }//GEN-LAST:event_cancelarActionPerformed

    private void finalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizarActionPerformed
        JOptionPane.showMessageDialog(null, "Venda concluída!");
        this.dispose();       
        
    }//GEN-LAST:event_finalizarActionPerformed

    private void selecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selecionarActionPerformed
                String novoNome = JOptionPane.showInputDialog("Digite o nome do cliente:");
        try {
                BufferedReader arquivo = new BufferedReader(new FileReader("cadastros/"+novoNome+".txt"));
                textoNome.setText(arquivo.readLine());
                textoTelefone.setText(arquivo.readLine());
                textoEndereco.setText(arquivo.readLine());
                arquivo.close();
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Cliente não encontrado!");
        }
    }//GEN-LAST:event_selecionarActionPerformed

    private void limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limparActionPerformed
        textoNome.setText("");
        textoTelefone.setText("");
        textoEndereco.setText("");
    }//GEN-LAST:event_limparActionPerformed
    float total = 0;
    
    private void adicionarVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarVendaActionPerformed
        Produto produto = new Produto();
        DefaultTableModel dtmProduto = (DefaultTableModel) listaVenda.getModel();
        
        try {

            BufferedReader arquivo = new BufferedReader(new FileReader("produtos/"+textoProduto.getText()+".txt"));
            produto.setNome(arquivo.readLine());
            produto.setTipo(arquivo.readLine());
            produto.setQuantidade(Integer.parseInt(arquivo.readLine()));
            produto.setPreco(Float.parseFloat(arquivo.readLine()));
            arquivo.close();
            
            
            if (Integer.valueOf(selecionarQuantidade.getValue().toString()) > 0) {    
                Object[] dados = {produto.getNome(), produto.getTipo(), selecionarQuantidade.getValue(), produto.getPreco(), produto.getPreco()*Float.parseFloat(selecionarQuantidade.getValue().toString())};
                dtmProduto.addRow(dados);
                
                total += produto.getPreco()*Float.parseFloat(selecionarQuantidade.getValue().toString()); 
                textoTotal.setText(String.valueOf(total));
                textoProduto.setText("");
                selecionarQuantidade.setValue(0);
            } else{
                JOptionPane.showMessageDialog(null, "Quantidade não selecionada!");
            }
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Produto não encontrado!");
        }
        
    }//GEN-LAST:event_adicionarVendaActionPerformed

    private void textoTrocoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoTrocoActionPerformed

    }//GEN-LAST:event_textoTrocoActionPerformed
    
    float troco = 0;
    private void gerarTrocoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gerarTrocoActionPerformed
        if (tipoPagamento.getSelectedItem().equals("Dinheiro")) {
            if(Float.parseFloat(textoReceber.getText()) >= Float.parseFloat(textoTotal.getText())) {
            troco = Float.parseFloat(textoReceber.getText())-Float.parseFloat(textoTotal.getText());
            textoTroco.setText(String.valueOf(troco));
            } else{
                JOptionPane.showMessageDialog(null, "Valor a receber é menor que o valor da venda!");
            }
        } else {
            textoTroco.setText("");
            textoReceber.setText(""); 
        }
    }//GEN-LAST:event_gerarTrocoActionPerformed

    private void retirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retirarActionPerformed
        ((DefaultTableModel) listaVenda.getModel()).setNumRows(0);
        textoTotal.setText("");
        total = 0;
    }//GEN-LAST:event_retirarActionPerformed

    private void textoNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoNomeActionPerformed
        
    }//GEN-LAST:event_textoNomeActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vender().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adicionarVenda;
    private javax.swing.JButton cancelar;
    private javax.swing.JButton finalizar;
    private javax.swing.JButton gerarTroco;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelEndereco;
    private javax.swing.JLabel labelLista;
    private javax.swing.JLabel labelNome;
    private javax.swing.JLabel labelProduto;
    private javax.swing.JLabel labelQuantidade;
    private javax.swing.JLabel labelReceber;
    private javax.swing.JLabel labelTelefone;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel labelTroco;
    private javax.swing.JButton limpar;
    private javax.swing.JTable listaVenda;
    private javax.swing.JButton retirar;
    private javax.swing.JButton selecionar;
    private javax.swing.JSpinner selecionarQuantidade;
    private javax.swing.JTextField textoEndereco;
    private javax.swing.JTextField textoNome;
    private javax.swing.JTextField textoProduto;
    private javax.swing.JTextField textoReceber;
    private javax.swing.JTextField textoTelefone;
    private javax.swing.JTextField textoTotal;
    private javax.swing.JTextField textoTroco;
    private javax.swing.JComboBox<String> tipoPagamento;
    // End of variables declaration//GEN-END:variables
}
